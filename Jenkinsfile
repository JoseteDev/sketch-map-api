pipeline {

    agent any

    stages {
        stage('Build') {
            when {
                allOf {
                    expression {
                        env.GIT_BRANCH == 'origin/master'
                    }
                }
            }
            steps {
                sh "echo 'Building API image...'"
                sh "docker build -t sm-api:beta /var/sketch-map/sketch-map-api"
            }
        }

        stage('Test') {
            when {
                allOf {
                    expression {
                        env.GIT_BRANCH == 'origin/master'
                    }
                }
            }
            steps {
                script {
                    try {
                        sh "docker rm -f sm-api-beta"
                    } catch (Exception e) {
                        sh "echo 'API container not found in beta environment D:'"
                    }
                }
                sh "echo 'Deploying Beta environment...'"
                sh "docker-compose -f /var/sketch-map/beta.yml up -d --no-recreate"
                sh "echo 'Running API tests...'"
                sh "docker-compose -f /var/sketch-map/api-test.yml up" // Without -d to get stdout and wait to tests
            }
            post {
                always {
                    sh "docker cp sm-api-test:/usr/src/app/reports /var/jenkins_home/workspace/API"
                    junit skipPublishingChecks: true, testResults: 'reports/*.xml'
                    sh "echo 'Removing testing container...'"
                    sh "docker rm -f sm-api-test"
                }
            }
        }

        stage('Deploy') {
            when {
                allOf {
                    expression {
                        env.GIT_BRANCH == 'origin/stable'
                    }
                }
            }
            steps {
                sh "echo 'Deploying API...'"
                sh "docker build -t sm-api /var/sketch-map/sketch-map-api"
                script {
                    try {
                        sh "docker rm -f sm-api"
                    } catch (Exception e) {
                        sh "echo 'API container not found in production environment D:'"
                    }
                }
                sh "docker-compose -f /var/sketch-map/production.yml up -d --no-recreate"
            }
        }
    }

    post {
        always {
            chuckNorris()
        }
    }
}
