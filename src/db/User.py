from . import db, ma


class User(db.Model):
    __tablename__ = 'User'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), unique=True, nullable=False)
    username = db.Column(db.String(80), nullable=False)
    password = db.Column(db.String(80), nullable=False)
    admin = db.Column(db.Boolean, nullable=False)
    sketch_list = db.relationship('Sketch', backref='User', lazy=True)

    def __init__(self, json):
        self.email = json.get('email')
        self.username = json.get('username')
        self.password = json.get('password', 'Patata100')
        self.admin = json.get('admin', False)
        self.sketch_list = []

    @classmethod
    def find_all(cls):
        return cls.query.all()

    @classmethod
    def find_by_id(cls, user_id):
        return cls.query.filter_by(id=user_id).first()

    @classmethod
    def find_by_email(cls, user_email):
        return cls.query.filter_by(email=user_email).first()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self):
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()


class UserSchema(ma.Schema):
    class Meta:
        fields = ('id', 'email', 'username', 'admin')  # fields to get
