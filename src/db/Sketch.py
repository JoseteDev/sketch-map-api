from . import db, ma


class Sketch(db.Model):
    __tablename__ = 'Sketch'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    longitude = db.Column(db.Float, nullable=False)
    latitude = db.Column(db.Float, nullable=False)
    zip_file = db.Column(db.LargeBinary(length=(2**32)-1), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('User.id'), nullable=False)

    def __init__(self, form=None, files=None, json=None):
        if form and files:
            self.name = form.get('name')
            self.longitude = form.get('longitude')
            self.latitude = form.get('latitude')
            self.zip_file = files.get('zip_file').read()
        elif json:
            self.name = json.get('name')
            self.longitude = json.get('longitude')
            self.latitude = json.get('latitude')
        else:
            pass

    @classmethod
    def find_all(cls):
        return cls.query.all()

    @classmethod
    def find_by_id(cls, sketch_id):
        return cls.query.filter_by(id=sketch_id).first()

    @classmethod
    def find_all_by_user_id(cls, user_id):
        return cls.query.filter_by(user_id=user_id)

    @classmethod
    def find_by_id_and_user_id(cls, sketch_id, user_id):
        return cls.query.filter_by(id=sketch_id, user_id=user_id).first()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self):
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()


class SketchSchema(ma.Schema):
    class Meta:
        fields = ('id', 'user_id', 'name', 'longitude', 'latitude')  # fields to get
