from flask import request
from flask_jwt_extended import jwt_required, get_jwt_identity
from sketches import sketches_bp, services
from utils import api_response_ok


# GETS

@sketches_bp.route('/sketches', methods=['GET'])
@jwt_required
def get_sketch_list():
    user_id = get_jwt_identity()
    sketch_list = services.get_sketch_list(user_id)
    return api_response_ok({'sketch_list': sketch_list})


@sketches_bp.route('/sketches/<sketch_id>', methods=['GET'])
@jwt_required
def get_sketch_by_id(sketch_id):
    user_id = get_jwt_identity()
    sketch = services.get_sketch_by_id(user_id, sketch_id)
    return api_response_ok({'sketch': sketch})


@sketches_bp.route('/sketches/<sketch_id>/<file_name>', methods=['GET'])
@jwt_required
def get_sketch_scene_file(sketch_id, file_name):
    user_id = get_jwt_identity()
    return services.get_sketch_file(user_id, sketch_id, file_name)  # Standard response to be used in THREE loader


@sketches_bp.route('/sketches/<sketch_id>/textures/<file_name>', methods=['GET'])
@jwt_required
def get_sketch_texture_file(sketch_id, file_name):
    user_id = get_jwt_identity()
    return services.get_sketch_file(user_id, sketch_id, 'textures/' + file_name)  # Standard response to be used in THREE loader


# POSTS

@sketches_bp.route('/sketches', methods=['POST'])
@jwt_required
def create_sketch():
    user_id = get_jwt_identity()
    sketch = services.create_sketch(user_id, request.form, request.files)
    return api_response_ok({'sketch': sketch})


# PUTS

@sketches_bp.route('/sketches/<sketch_id>', methods=['PUT'])
@jwt_required
def update_sketch(sketch_id):
    user_id = get_jwt_identity()
    sketch = services.update_sketch(user_id, sketch_id, request.json)
    return api_response_ok({'sketch': sketch})


# DELETES

@sketches_bp.route('/sketches/<sketch_id>', methods=['DELETE'])
@jwt_required
def delete_sketch(sketch_id):
    user_id = get_jwt_identity()
    sketch = services.delete_sketch(user_id, sketch_id)
    return api_response_ok({'sketch': sketch})
