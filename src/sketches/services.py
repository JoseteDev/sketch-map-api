import tempfile
import zipfile

from db.Sketch import Sketch, SketchSchema
from db.User import User

sketch_schema = SketchSchema()
sketch_list_schema = SketchSchema(many=True)


def get_sketch_list(user_id):
    sketch_list = Sketch.find_all_by_user_id(user_id)
    return sketch_list_schema.dump(sketch_list)


def get_sketch_by_id(user_id, sketch_id):
    sketch = Sketch.find_by_id_and_user_id(sketch_id, user_id)
    return sketch_schema.dump(sketch)


def get_sketch_file(user_id, sketch_id, file_path):
    sketch = Sketch.find_by_id_and_user_id(sketch_id, user_id)
    with tempfile.NamedTemporaryFile(suffix='.zip') as t:
        tmp_file = open(t.name, 'wb')
        tmp_file.write(sketch.zip_file)
        archive = zipfile.ZipFile(t.name, 'r')
    return archive.read(file_path)


def create_sketch(user_id, form_sketch, files):
    sketch = Sketch(form=form_sketch, files=files)
    user = User.find_by_id(user_id)
    user.sketch_list.append(sketch)
    user.save()
    sketch.save()
    return sketch_schema.dump(sketch)


def update_sketch(user_id, sketch_id, json_sketch):
    sketch = Sketch.find_by_id_and_user_id(sketch_id, user_id)
    new_sketch = Sketch(json=json_sketch)

    if new_sketch.name:
        sketch.name = new_sketch.name
    if new_sketch.longitude:
        sketch.longitude = new_sketch.longitude
    if new_sketch.latitude:
        sketch.latitude = new_sketch.latitude

    # File and UserID not updated

    sketch.update()
    return sketch_schema.dump(sketch)


def delete_sketch(user_id, sketch_id):
    sketch = Sketch.find_by_id_and_user_id(sketch_id, user_id)
    sketch.delete()
    return sketch_schema.dump(sketch)
