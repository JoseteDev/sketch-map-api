from flask import request
from flask_jwt_extended import create_access_token, jwt_required, get_raw_jwt

from users.services import get_user_by_email_and_password
from auth import auth_bp, blacklist
from utils import api_response


@auth_bp.route('/auth/login', methods=['POST'])
def login():
    email = request.json.get('email', None)
    password = request.json.get('password', None)
    if not email or not password:
        return api_response(400, 'Bad request', None)

    user = get_user_by_email_and_password(email, password)
    if user:
        token = create_access_token(identity=user.get('id'))
        return api_response(200, 'Welcome back!', {'jwt': token, 'user': user})
    else:
        return api_response(401, 'Unauthorized', None)


@auth_bp.route('/auth/logout', methods=['POST'])
@jwt_required
def logout():
    jti = get_raw_jwt()['jti']
    blacklist.add(jti)
    return api_response(200, 'See you soon!', None)
