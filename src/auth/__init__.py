from flask import Blueprint
from flask_jwt_extended import JWTManager, verify_jwt_in_request, get_jwt_identity
from functools import wraps
from db.User import User
from utils import api_response


auth_bp = Blueprint('auth', __name__)
jwt = JWTManager()
blacklist = set()


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return jti in blacklist


def admin_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        user_id = get_jwt_identity()
        user = User.find_by_id(user_id)
        if user.admin:
            return fn(*args, **kwargs)
        else:
            return api_response(403, 'Forbidden', None)
    return wrapper
