import json


def api_response(status, message, data):
    return {
        'status': status,
        'message': message,
        'data': data
    }, status


def api_response_ok(data):
    return api_response(200, 'Success', data)
