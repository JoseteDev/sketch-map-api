from werkzeug.security import generate_password_hash, check_password_hash
from db.User import User, UserSchema


user_schema = UserSchema()
user_list_schema = UserSchema(many=True)


def get_all_users():
    user_list = User.find_all()
    return user_list_schema.dump(user_list)


def get_user_by_id(user_id):
    user = User.find_by_id(user_id)
    return user_schema.dump(user)


def get_user_by_email_and_password(email, password):
    user = User.query.filter_by(email=email).first()
    if user and check_password_hash(user.password, password):
        return user_schema.dump(user)
    else:
        return None


def create_user(json_user):
    user = User(json_user)
    user.password = generate_password_hash(user.password, method='sha256')
    user.save()
    return user_schema.dump(user)


def update_user(user_id, json_user):
    user = User.find_by_id(user_id)
    new_user = User(json_user)

    if new_user.email is not None:
        user.email = new_user.email
    if new_user.username is not None:
        user.username = new_user.username
    if new_user.admin is not None:
        user.admin = new_user.admin

    # Password not updated

    user.update()
    return user_schema.dump(user)


def delete_user(user_id):
    user = User.find_by_id(user_id)
    user.delete()
    return user_schema.dump(user)
