from flask import request
from flask_jwt_extended import jwt_required, get_jwt_identity
from users import users_bp, services
from auth import admin_required
from utils import api_response, api_response_ok


@users_bp.route('/users', methods=['GET'])
@jwt_required
@admin_required
def get_user_list():
    user_list = services.get_all_users()
    return api_response_ok({'user_list': user_list})


@users_bp.route('/users/<user_id>', methods=['GET'])
@jwt_required
def get_user_by_id(user_id):
    if int(user_id) is not get_jwt_identity():
        return api_response(403, 'Forbidden', None)
    user = services.get_user_by_id(user_id)
    return api_response_ok({'user': user})


@users_bp.route('/users', methods=['POST'])
def create_user():
    user = services.create_user(request.json)
    return api_response_ok({'user': user})


@users_bp.route('/users/<user_id>', methods=['PUT'])
@jwt_required
@admin_required
def update_user(user_id):
    user = services.update_user(user_id, request.json)
    return api_response_ok({'user': user})


@users_bp.route('/users/<user_id>', methods=['DELETE'])
@jwt_required
@admin_required
def delete_user(user_id):
    user = services.delete_user(user_id)
    return api_response_ok({'user': user})
