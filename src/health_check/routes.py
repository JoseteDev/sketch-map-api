from health_check import health_check_bp
from utils import api_response
from auth import admin_required
from flask_jwt_extended import jwt_required


@health_check_bp.route('/', methods=['GET'])
def main():
    paths = [
        '/',
        '/health-check',
        '/ping',
        '/ping/auth',
        '/ping/admin',
        '/auth/login',
        '/auth/logout',
        '/auth/ping',
        '/users',
        '/sketches'
    ]
    return api_response(200, 'Server is up!', {'paths': paths})


@health_check_bp.route('/ping', methods=['GET'])
def ping():
    return api_response(200, 'Pong! :D', None)


@health_check_bp.route('/ping/auth', methods=['GET'])
@jwt_required
def auth_ping():
    return api_response(200, 'Auth-Pong! :D', None)


@health_check_bp.route('/ping/admin', methods=['GET'])  # @route must always be the outer-most decorator
@jwt_required
@admin_required
def admin_ping():
    return api_response(200, 'Admin-Pong! :D', None)
