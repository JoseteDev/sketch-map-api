from flask import Flask
from flask_cors import CORS
from db import db, ma
from auth import jwt
import os


app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = 'th1s-1s-4-m3g4-s3cr3t-k3y'
app.config['JWT_TOKEN_LOCATION'] = ['headers', 'query_string']
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access']
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DB_URL', 'mysql+pymysql://flask:flask@localhost/sketch-map')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

jwt.init_app(app)
db.init_app(app)
ma.init_app(app)


from db import User, Sketch
db.create_all(app=app)


from users import users_bp, routes
app.register_blueprint(users_bp)

from sketches import sketches_bp, routes
app.register_blueprint(sketches_bp)

from auth import auth_bp, routes
app.register_blueprint(auth_bp)

from health_check import health_check_bp, routes
app.register_blueprint(health_check_bp)


CORS(app)


if __name__ == '__main__':
    app.run(
        host=os.environ.get('API_HOST', 'localhost'),
        port=5000,
        debug=True
    )
